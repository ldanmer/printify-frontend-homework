import {Injectable} from '@angular/core';
import {getStatusText, InMemoryDbService, ResponseOptions, STATUS, RequestInfo} from 'angular-in-memory-web-api';
import {Observable} from 'rxjs';
import {orders} from './orders.data';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() {
  }

  createDb(): {} | Observable<{}> | Promise<{}> {
    return {orders};
  }

  // HTTP GET interceptor
  get(reqInfo: RequestInfo): any {
    // @ts-ignore
    const requestParams = reqInfo.req.params;
    if (requestParams.has('page')) {
      const page = requestParams.get('page');
      const limit = requestParams.has('limit') ? requestParams.get('limit') : environment.pageSize;
      reqInfo.query.delete('page');
      reqInfo.query.delete('limit');
      return reqInfo.utils.createResponse$(() => {
        const collection = reqInfo.collection as Array<{ id: any }>;
        const filteredCollection = this.applyQuery(collection, reqInfo.query);
        const data = filteredCollection.slice(limit * (page - 1), limit * page);
        const options: ResponseOptions = data ?
          {
            body: data,
            status: STATUS.OK
          } :
          {
            body: {error: `${reqInfo.collectionName} with id=${reqInfo.id} not found`},
            status: STATUS.NOT_FOUND
          };
        return this.finishOptions(options, reqInfo);
      });
    }
  }

  private finishOptions(options: ResponseOptions, {headers, url}: RequestInfo): ResponseOptions {
    options.statusText = getStatusText(options.status);
    options.headers = headers;
    options.url = url;
    return options;
  }

  private applyQuery(collection: Array<object>, query: Map<string, string[]>): Array<object> | object[] {
    const conditions = [];
    query.forEach((values, key) => {
      values.forEach(value => {
        conditions.push({key, rx: new RegExp(decodeURI(value))});
      });
    });
    if (!conditions.length) {
      return collection;
    }
    return collection.filter(row => {
      let ok = true;
      let i = conditions.length;
      while (ok && i--) {
        const condition = conditions[i];
        ok = condition.rx.test(row[condition.key]);
      }
      return ok;
    });
  }
}
