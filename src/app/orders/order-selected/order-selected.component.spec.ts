import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderSelectedComponent} from './order-selected.component';
import {provideMockStore} from '@ngrx/store/testing';
import {By} from '@angular/platform-browser';
import {Order} from '../order';

describe('OrderSelectedComponent', () => {
  let component: OrderSelectedComponent;
  let fixture: ComponentFixture<OrderSelectedComponent>;
  const initialState = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderSelectedComponent],
      providers: [provideMockStore({initialState})]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit event on unselectOrderHandler call', () => {
    spyOn(component.returnToFirstStep, 'emit');
    component.unselectOrderHandler();
    expect(component.returnToFirstStep.emit).toHaveBeenCalledWith(true);
  });

  it('should call unselectOrderHandler on button click', () => {
    spyOn(component, 'unselectOrderHandler');
    component.selectedOrder = {name: 'mockOrder'} as unknown as Order;
    fixture.detectChanges();
    fixture.debugElement.query(By.css('button.integration--unselect-order')).triggerEventHandler('click', {});
    expect(component.unselectOrderHandler).toHaveBeenCalled();
  });
});
