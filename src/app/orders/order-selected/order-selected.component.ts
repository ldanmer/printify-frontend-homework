import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Store} from '@ngrx/store';
import * as formOrder from '../state';
import * as orderActions from '../state/order.actions';
import {Order} from '../order';
import * as fromProduct from '../../products/state';
import * as productActions from '../../products/state/product.actions';

@Component({
  selector: 'app-order-selected',
  templateUrl: './order-selected.component.html',
  styleUrls: ['./order-selected.component.scss']
})
export class OrderSelectedComponent implements OnInit {
  @Input() selectedOrder: Order;
  @Output() returnToFirstStep = new EventEmitter<boolean>();

  constructor(private storeOrder: Store<formOrder.State>, private storeProduct: Store<fromProduct.State>) {
  }

  ngOnInit(): void {
  }


  unselectOrderHandler(): void {
    this.storeOrder.dispatch(new orderActions.Clear());
    this.storeProduct.dispatch(new productActions.Clear());
    this.returnToFirstStep.emit(true);
  }
}
