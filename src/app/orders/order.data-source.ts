import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Order} from './order';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {OrderService} from './order.service';
import {environment} from '../../environments/environment';
import {catchError, finalize} from 'rxjs/operators';

export class OrderDataSource implements DataSource<Order> {

  private ordersSubject = new BehaviorSubject<Order[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private orderService: OrderService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<Order[]> {
    return this.ordersSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.ordersSubject.complete();
    this.loadingSubject.complete();
  }

  loadOrders(page = 1, limit = environment.pageSize, listed = true): void {

    this.loadingSubject.next(true);

    this.orderService.getListedOrders(page, limit, listed)
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      ).subscribe(orders => this.ordersSubject.next(orders));
  }
}
