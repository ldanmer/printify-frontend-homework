import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {OrderFindComponent} from './order-find.component';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {OrderService} from '../order.service';
import {OrderServiceMock} from '../../testing/mocks/order.service.mock';

describe('OrderFindComponent', () => {
  let component: OrderFindComponent;
  let fixture: ComponentFixture<OrderFindComponent>;
  let httpClientTestingController: HttpTestingController;
  let store: MockStore;

  const initialState = {order: {}};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderFindComponent],
      imports: [HttpClientTestingModule],
      providers: [
        provideMockStore({initialState}),
        {provide: OrderService, useClass: OrderServiceMock}
      ]
    })
      .compileComponents();
    httpClientTestingController = TestBed.inject(HttpTestingController);
    store = TestBed.inject(MockStore);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should search order', fakeAsync (() => {
    const id = 1236;
    component.orderSearch(id.toString());
    tick(300);
    expect(component.filteredOrders[0].id).toEqual(id);
  }));
});
