import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderFindListComponent } from './order-find-list.component';
import {provideMockStore} from '@ngrx/store/testing';
import {MatTableDataSource} from '@angular/material/table';
import {Order} from '../../order';

describe('OrderFindListComponent', () => {
  let component: OrderFindListComponent;
  let fixture: ComponentFixture<OrderFindListComponent>;
  const initialState = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFindListComponent ],
      providers: [provideMockStore({initialState})]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFindListComponent);
    component = fixture.componentInstance;
    component.dataSource = {} as MatTableDataSource<Order>;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
