import {AfterViewInit, Component, Input, OnChanges, ViewChild} from '@angular/core';
import {Order} from '../../order';
import {Store} from '@ngrx/store';
import * as formOrder from '../../state';
import * as orderActions from '../../state/order.actions';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';


@Component({
  selector: 'app-order-find-list',
  templateUrl: './order-find-list.component.html',
  styleUrls: ['./order-find-list.component.scss'],
})
export class OrderFindListComponent implements OnChanges, AfterViewInit {

  dataSource: MatTableDataSource<Order>;
  @Input() filteredOrders: Order[];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'customer', 'products', 'volume', 'sku'];

  constructor(private storeOrder: Store<formOrder.State>) {
  }

  ngOnChanges(): void {
    this.dataSource = new MatTableDataSource<Order>(this.filteredOrders);
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  selectOrder(order: Order): void {
    this.storeOrder.dispatch(new orderActions.Add(order));
  }
}
