import {Component, OnDestroy, OnInit} from '@angular/core';
import {Order} from '../order';
import {OrderService} from '../order.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, takeWhile} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import * as formOrder from '../state';

@Component({
  selector: 'app-order-find',
  templateUrl: './order-find.component.html',
  styleUrls: ['./order-find.component.scss']
})
export class OrderFindComponent implements OnInit, OnDestroy {

  filteredOrders: Order[];
  selectedOrder: Order;

  private searchString = new Subject<string>();
  componentActive = true;

  constructor(private orderService: OrderService, private storeOrder: Store<formOrder.State>) {
  }

  ngOnInit(): void {
    this.searchString.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(value => this.orderService.searchUnlistedOrdersById(value))
    ).subscribe(
      orders => this.filteredOrders = orders
    );

    this.storeOrder.pipe(
      takeWhile(() => this.componentActive),
      select(formOrder.getOrder)
    ).subscribe(
      order => this.selectedOrder = order
    );

    this.searchString.next(null);
  }

  orderSearch(value: string): void {
    this.searchString.next(value);
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }
}
