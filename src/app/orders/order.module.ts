import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderListComponent} from './order-list/order-list.component';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {OrderImportComponent} from './order-import/order-import.component';
import {MatDialogModule} from '@angular/material/dialog';
import {OrderFindComponent} from './order-find/order-find.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {ProductModule} from '../products/product.module';
import {OrderConfirmComponent} from './order-confirm/order-confirm.component';
import {OrderFindListComponent} from './order-find/order-find-list/order-find-list.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { OrderSelectedComponent } from './order-selected/order-selected.component';
import {StoreModule} from '@ngrx/store';
import {reducer} from './state/order.reducer';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    OrderListComponent,
    OrderImportComponent,
    OrderFindComponent,
    OrderConfirmComponent,
    OrderFindListComponent,
    OrderSelectedComponent
  ],
  exports: [
    OrderListComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    ProductModule,
    MatButtonToggleModule,
    StoreModule.forFeature('order', reducer),
    MatListModule,
    MatCardModule,
    FormsModule,
    MatGridListModule,
    MatIconModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
  ]
})
export class OrderModule {
}
