import {Product} from '../products/product';

export interface Order {
  id: number;
  customer: string;
  created: Date;
  revenue: number;
  cost: number;
  price: number;
  fulfillment: Fulfillment;
  products: Product[];
  listed: boolean;
  volume: number;
  SKU: string;
}

export enum Fulfillment {
  InProduction = 'In Production',
  QualityControl = 'Quality Control',
  Completed = 'Completed',
  Pending = 'Pending',
}
