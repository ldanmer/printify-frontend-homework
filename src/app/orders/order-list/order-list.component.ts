import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Order} from '../order';
import {MatDialog} from '@angular/material/dialog';
import {OrderImportComponent} from '../order-import/order-import.component';
import {MatPaginator} from '@angular/material/paginator';
import {OrderDataSource} from '../order.data-source';
import {OrderService} from '../order.service';
import {count, map, tap} from 'rxjs/operators';
import {logger} from 'codelyzer/util/logger';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit, AfterViewInit {

  @Input() dataSource: OrderDataSource;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'customer', 'created', 'revenue', 'cost', 'price', 'fulfillment'];
  listedOrdersCount: number;

  constructor(public dialog: MatDialog, private orderService: OrderService) {
  }

  openDialog(): void {
    this.dialog.open(OrderImportComponent, {
      width: '100%'
    });
  }

  ngAfterViewInit(): void {
    this.paginator.page.pipe(
      tap(() => this.loadOrdersPage())
    ).subscribe();
  }


  ngOnInit(): void {
    this.orderService.getAllOrders().pipe(
      map(orders => {
        return orders.filter(order => !!order.listed);
      }),
    ).subscribe(
      ordersCount => this.listedOrdersCount = ordersCount.length
    );
  }

  loadOrdersPage(): void {
    this.dataSource.loadOrders(this.paginator.pageIndex + 1, this.paginator.pageSize);
  }
}
