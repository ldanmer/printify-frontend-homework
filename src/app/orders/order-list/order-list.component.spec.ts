import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderListComponent} from './order-list.component';
import {MatDialogModule} from '@angular/material/dialog';
import {By} from '@angular/platform-browser';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';
import {OrderDataSource} from '../order.data-source';
import {MatPaginatorModule} from '@angular/material/paginator';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('OrderListComponent', () => {
  let component: OrderListComponent;
  let fixture: ComponentFixture<OrderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderListComponent],
      imports: [MatDialogModule, HttpClientTestingModule, MatPaginatorModule, BrowserAnimationsModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderListComponent);
    component = fixture.componentInstance;
    component.dataSource = {
      loading$: of(false)
    } as OrderDataSource;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open a dialog modal', () => {
    spyOn(component, 'openDialog');
    fixture.debugElement.query(By.css('button')).triggerEventHandler('click', {});
    expect(component.openDialog).toHaveBeenCalled();
  });
});
