import {Action} from '@ngrx/store';
import {Order} from '../order';

export enum OrderActionTypes {
  Add = '[Order] Add',
  Clear = '[Order] Clear'
}

export class Add implements Action {
  readonly type = OrderActionTypes.Add;

  constructor(public payload: Order) {
  }
}

export class Clear implements Action {
  readonly type = OrderActionTypes.Clear;

  constructor() {
  }
}

export type OrderActions = Add
  | Clear;


