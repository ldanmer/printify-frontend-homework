import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../state/app.state';
import * as fromOrders from './order.reducer';

export interface State extends fromRoot.State {
  orders: fromOrders.OrderState;
}

// Selector functions
const getOrderFeatureState = createFeatureSelector<fromOrders.OrderState>('order');

export const getOrder = createSelector(
  getOrderFeatureState,
  state => state.order
);

export const getError = createSelector(
  getOrderFeatureState,
  state => state.error
);
