import {OrderActionTypes, OrderActions} from './order.actions';
import {Order} from '../order';

export interface OrderState {
  order: Order;
  error: string;
}

const initialState: OrderState = {
  order: null,
  error: ''
};

export function reducer(state = initialState, action: OrderActions): OrderState {
  switch (action.type) {
    case OrderActionTypes.Add:
      return {
        ...state,
        order: action.payload

      };

    case OrderActionTypes.Clear:
      return {
        ...state,
        order: null
      };

    default:
      return state;
  }
}
