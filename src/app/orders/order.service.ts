import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Order} from './order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private ordersUrl = 'api/orders';

  constructor(private httpClient: HttpClient) {
  }

  getListedOrders(page, limit, listed): Observable<Order[]> {
    const params = new HttpParams()
      .set('listed', listed)
      .set('page', page)
      .set('limit', limit);
    return this.httpClient.get<Order[]>(this.ordersUrl, {params})
      .pipe(
        catchError(this.handleError<Order[]>('getOrders', []))
      );
  }

  searchUnlistedOrdersById(id: string): Observable<Order[]> {
    let params = new HttpParams().set('listed', 'false');
    if (!!id) {
      params = params.append('id', id);
    }
    return this.httpClient.get<Order[]>(`${this.ordersUrl}`, {params})
      .pipe(
        catchError(this.handleError<Order[]>(`searchUnlistedOrdersById id=${id}`))
      );
  }

  getAllOrders(): Observable<Order[]> {
    return this.httpClient.get<Order[]>(this.ordersUrl).pipe(
      catchError(this.handleError<Order[]>('getAllOrders', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
