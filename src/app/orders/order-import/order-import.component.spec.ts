import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {OrderImportComponent} from './order-import.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {productsStub} from '../../testing/mocks/products.stub';
import {MemoizedSelector} from '@ngrx/store';
import * as fromProduct from '../../products/state';
import * as fromOrder from '../state';
import {Product} from '../../products/product';
import {Order} from '../order';
import {ordersStub} from '../../testing/mocks/order.service.mock';
import {By} from '@angular/platform-browser';
import createSpyObj = jasmine.createSpyObj;
import {OrderConfirmComponent} from '../order-confirm/order-confirm.component';
import {FormsModule} from '@angular/forms';


describe('OrderImportComponent', () => {
  describe('Integration', () => {
    let component: OrderImportComponent;
    let fixture: ComponentFixture<OrderImportComponent>;
    let store: MockStore;
    let mockProductsSelector: MemoizedSelector<fromProduct.State, Product[]>;
    let mockOrderSelector: MemoizedSelector<fromOrder.State, Order>;
    const initialState = {products: []};

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [OrderImportComponent, OrderConfirmComponent],
        imports: [MatDialogModule, FormsModule],
        providers: [
          {provide: MatDialogRef, useValue: {}},
          {provide: MAT_DIALOG_DATA, useValue: {}},
          provideMockStore({initialState})
        ]
      })
        .compileComponents();
      store = TestBed.inject(MockStore);
      mockProductsSelector = store.overrideSelector(
        fromProduct.getOrderedProducts,
        productsStub
      );

      mockOrderSelector = store.overrideSelector(
        fromOrder.getOrder,
        ordersStub[0]
      );
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(OrderImportComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should has ordered products', () => {
      expect(component.orderedProducts.length).toEqual(3);
      expect(component.orderedProducts).toEqual(productsStub);
    });

    it('should has selected order', () => {
      expect(component.selectedOrder).toEqual(ordersStub[0]);
    });

    it('should call unselectOrder', () => {
      spyOn(component, 'unselectOrder');
      fixture.debugElement.query(By.css('button.integration--unselect-order')).triggerEventHandler('click', {});
      fixture.detectChanges();
      expect(component.unselectOrder).toHaveBeenCalled();
    });

    it('should call clearOrdered', () => {
      spyOn(component, 'clearOrdered');
      fixture.debugElement.query(By.css('button.integration--clear-ordered')).triggerEventHandler('click', {});
      fixture.detectChanges();
      expect(component.clearOrdered).toHaveBeenCalled();
    });

    it('should clear order on clearOrdered call', () => {
      spyOn(store, 'dispatch');
      component.clearOrdered();
      fixture.detectChanges();
      expect(component).toBeTruthy();
      expect(store.dispatch).toHaveBeenCalledTimes(1);
    });

    it('should unselect order and clear ordered products unselectOrder call', () => {
      spyOn(store, 'dispatch');
      component.unselectOrder();
      fixture.detectChanges();
      expect(component).toBeTruthy();
      expect(store.dispatch).toHaveBeenCalledTimes(2);
    });

    it('should return on the first step on onReturnToFirstStepHandler call', () => {
      const stepper = createSpyObj('MatStepperMock', ['previous']);
      stepper.previous.and.returnValue(true);
      component.stepper = stepper;
      component.onReturnToFirstStepHandler(true);
      fixture.detectChanges();
      expect(component).toBeTruthy();
    });

    it('should resets order form', () => {
      spyOn(component.orderConfirm.orderForm, 'resetForm');
      component.orderFormReset();
      fixture.detectChanges();
      expect(component.orderConfirm.orderForm.resetForm).toHaveBeenCalled();
    });

    it('should submit the form on valid', () => {
      spyOnProperty(component.orderConfirm.orderForm, 'valid').and.returnValue(true);
      component.submitOrder();
      fixture.detectChanges();
      expect(component.isOrderCompleted).toEqual(true);

    });

  });
});
