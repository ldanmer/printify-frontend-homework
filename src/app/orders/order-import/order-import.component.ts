import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Order} from '../order';
import {select, Store} from '@ngrx/store';
import * as fromProduct from '../../products/state';
import * as fromOrder from '../state';
import * as productActions from '../../products/state/product.actions';
import * as orderActions from '../state/order.actions';
import {Product, Variant} from '../../products/product';
import {takeWhile} from 'rxjs/operators';
import {OrderConfirmComponent} from '../order-confirm/order-confirm.component';
import {MatStepper} from '@angular/material/stepper';

@Component({
  selector: 'app-order-import',
  templateUrl: './order-import.component.html',
  styleUrls: ['./order-import.component.scss']
})
export class OrderImportComponent implements OnInit, OnDestroy {

  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild(OrderConfirmComponent) orderConfirm: OrderConfirmComponent;
  selectedProducts: Product[];
  orderedProducts: Product[];
  selectedOrder: Order;
  selectedVariants: Variant[];
  componentActive = true;
  isOrderCompleted: boolean;

  constructor(public dialogRef: MatDialogRef<OrderImportComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Order,
              private storeProduct: Store<fromProduct.State>,
              private storeOrder: Store<fromOrder.State>
  ) {
  }

  ngOnInit(): void {
    this.storeProduct
      .pipe(
        takeWhile(() => this.componentActive),
        select(fromProduct.getProducts)
      ).subscribe(
      products => this.selectedProducts = products
    );

    this.storeProduct
      .pipe(
        takeWhile(() => this.componentActive),
        select(fromProduct.getOrderedProducts)
      ).subscribe(
      products => {
        this.selectedVariants = [].concat.apply([], products.map(product => product.variants));
        return this.orderedProducts = products;
      }
    );

    this.storeOrder
      .pipe(
        takeWhile(() => this.componentActive),
        select(fromOrder.getOrder)
      ).subscribe(
      order => this.selectedOrder = order
    );
  }

  unselectOrder(): void {
    this.storeProduct.dispatch(new productActions.Clear());
    this.storeOrder.dispatch(new orderActions.Clear());
  }

  clearOrdered(): void {
    this.storeProduct.dispatch(new productActions.ClearOrder());
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }

  submitOrder(): void {
    this.orderConfirm.orderForm.onSubmit(new Event('submit'));
    if (this.orderConfirm.orderForm.valid) {
      this.isOrderCompleted = true;
    }
  }

  orderFormReset(): void {
    this.orderConfirm.orderForm.resetForm();
  }

  onReturnToFirstStepHandler($event: boolean): void {
    if ($event) {
      this.stepper.previous();
    }
  }
}
