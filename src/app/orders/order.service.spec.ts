import {async, TestBed} from '@angular/core/testing';

import {OrderService} from './order.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Order} from './order';

describe('OrderService', () => {
  let service: OrderService;
  let httpClientTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(OrderService);
    httpClientTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should handle error', async(() => {
    // @ts-ignore
    const handleError = service.handleError<Order>('getListedOrders', []);
    expect(handleError(error => console.log(error))).toBeTruthy();
  }));

  describe('getListedOrders', () => {
    it('should return listed orders only', () => {
      service.getListedOrders(1, 3, true).subscribe(response => expect(response.length).toEqual(2));
      httpClientTestingController.expectOne({
        method: 'GET',
        url: 'api/orders?listed=true&page=1&limit=3'
      });
    });
  });

  describe('searchUnlistedOrdersById', () => {
    it('should return all non listed orders', () => {
      service.searchUnlistedOrdersById('').subscribe(response => expect(response.length).toEqual(1));
      httpClientTestingController.expectOne({
        method: 'GET',
        url: 'api/orders?listed=false'
      });
    });

    it('should find non listed order by ID', () => {
      const id = '1236';
      service.searchUnlistedOrdersById(id).subscribe(response => expect(response.length).toEqual(1));
      httpClientTestingController.expectOne({
        method: 'GET',
        url: `api/orders?listed=false&id=${id}`
      });
    });
  });

  afterEach(() => {
    httpClientTestingController.verify();
  });

});
