import {Component, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import {Variant} from '../../products/product';
import {NgForm, NgModel} from '@angular/forms';

@Component({
  selector: 'app-order-confirm',
  templateUrl: './order-confirm.component.html',
  styleUrls: ['./order-confirm.component.scss']
})
export class OrderConfirmComponent implements OnChanges {

  @Input() selectedVariants: Variant[];
  @ViewChild('orderForm') orderForm: NgForm;
  @ViewChild('address') address: NgModel;
  totalSum: number;

  constructor() {
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (!!changes.selectedVariants.currentValue.length) {
      this.totalSum = this.selectedVariants.reduce((sum, variant) => sum + variant.price, 0);
    }
  }
}
