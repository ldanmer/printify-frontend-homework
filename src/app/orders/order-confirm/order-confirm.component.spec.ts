import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderConfirmComponent} from './order-confirm.component';
import {FormsModule} from '@angular/forms';
import {SockVariants} from '../../products/product';

describe('OrderConfirmComponent', () => {
  let component: OrderConfirmComponent;
  let fixture: ComponentFixture<OrderConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderConfirmComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return order sum', () => {
    component.selectedVariants = [
      {name: SockVariants.Light, price: 50},
      {name: SockVariants.HeavyWool, price: 100},
    ];

    const mockChanges: any = {
      selectedVariants: {
        currentValue: component.selectedVariants
      }
    };

    component.ngOnChanges(mockChanges);

    expect(component.totalSum).toEqual(150);
  });

});
