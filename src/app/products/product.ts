export interface Product {
  id: number;
  name: string;
  SKU: string;
  variants: Variant[];
}

export interface Variant {
  name: VarType;
  price: number;
}

export enum SockVariants {
  HeavyWool = 'Heavy Wool Socks',
  Crew = 'Crew Socks',
  Light = 'Light Socks',
}

export enum SomeVariants {}

type VarType = SockVariants | SomeVariants;
