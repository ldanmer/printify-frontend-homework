import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductsPrepareComponent} from './products-prepare/products-prepare.component';
import {ProductListComponent} from './product-list/product-list.component';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {StoreModule} from '@ngrx/store';
import {reducer} from './state/product.reducer';
import {MatIconModule} from '@angular/material/icon';
import {ProductVariantsComponent} from './product-variants/product-variants.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    ProductsPrepareComponent,
    ProductListComponent,
    ProductVariantsComponent
  ],
  exports: [
    ProductsPrepareComponent,
    ProductListComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatCheckboxModule,
    StoreModule.forFeature('products', reducer),
    MatIconModule,
    MatExpansionModule,
    MatRadioModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
  ]
})
export class ProductModule {
}
