import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductVariantsComponent} from './product-variants.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {MemoizedSelector} from '@ngrx/store';
import * as fromProduct from '../state';
import {Product, SockVariants, Variant} from '../product';
import {productsStub} from '../../testing/mocks/products.stub';

describe('ProductVariantsComponent', () => {
  let component: ProductVariantsComponent;
  let fixture: ComponentFixture<ProductVariantsComponent>;
  const initialState = {};
  let mockProductsSelector: MemoizedSelector<fromProduct.State, Product[]>;
  let store: MockStore;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductVariantsComponent],
      providers: [provideMockStore({initialState})]
    })
      .compileComponents();
    store = TestBed.inject(MockStore);

    mockProductsSelector = store.overrideSelector(
      fromProduct.getOrderedProducts,
      productsStub
    );
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductVariantsComponent);
    component = fixture.componentInstance;
    component.product = productsStub[0];
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should reset selectedValue on empty products array', () => {
    mockProductsSelector = store.overrideSelector(
      fromProduct.getOrderedProducts,
      []
    );
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.selectedValue).toBeNull();
  });

  it('should set filteredVariants on variantSearch', () => {
    const value = SockVariants.HeavyWool;
    component.variantSearch(value);
    fixture.detectChanges();
    expect(component.filteredVariants.length).toEqual(1);
  });

  it('should dispatch addToOrder onVariantSelect call', () => {
    spyOn(store, 'dispatch');
    const variant = {name: SockVariants.Crew, price: 5} as Variant;
    component.onVariantSelect(variant);
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });
});
