import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Product, Variant} from '../product';
import {select, Store} from '@ngrx/store';
import * as fromProduct from '../state';
import * as productActions from '../../products/state/product.actions';
import {takeWhile} from 'rxjs/operators';

@Component({
  selector: 'app-product-variants',
  templateUrl: './product-variants.component.html',
  styleUrls: ['./product-variants.component.scss']
})
export class ProductVariantsComponent implements OnInit, OnDestroy {
  @Input() product: Product;
  @ViewChild('variantsRadioButtonGroup') variantsRadioButtonGroup: ElementRef<boolean>;
  filteredVariants: Variant[];
  orderedProducts: Product[];
  selectedValue: string;
  private componentActive = true;

  constructor(private storeProduct: Store<fromProduct.State>) {
  }

  ngOnInit(): void {
    this.filteredVariants = this.product.variants;
    this.selectedValue = this.product.name;
    this.storeProduct.pipe(
      takeWhile(() => this.componentActive),
      select(fromProduct.getOrderedProducts)
    ).subscribe(
      products => {
        if (!products.length) {
          this.selectedValue = null;
        }
        return this.orderedProducts = products;
      }
    );
  }

  variantSearch(value: string): void {
    const regExp = new RegExp(value.toLowerCase());
    this.filteredVariants = this.product.variants.filter(variant => regExp.test((variant.name as string).toLowerCase()));
  }

  onVariantSelect(variant: Variant): void {
    const product = Object.assign({}, this.product, {variants: [variant]});
    this.storeProduct.dispatch(new productActions.AddToOrder(product));
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }
}
