import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../product';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {Store} from '@ngrx/store';
import * as fromProduct from '../state';
import * as productActions from '../state/product.actions';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  @Input() products: Product[];
  selectedProducts = new Set<Product>();
  displayedColumns: string[] = ['name', 'sku', 'checkbox'];

  constructor(private store: Store<fromProduct.State>) {
  }

  ngOnInit(): void {
  }

  onSelect($event: MatCheckboxChange, product: Product): void {
    if (!!$event.checked) {
      this.selectedProducts.add(product);
    } else {
      this.selectedProducts.delete(product);
    }
    this.store.dispatch(new productActions.Add(this.selectedProducts));
  }
}
