import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductListComponent} from './product-list.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {productsStub} from '../../testing/mocks/products.stub';
import {Product} from '../product';
import {MatCheckboxChange} from '@angular/material/checkbox';

describe('ProductListComponent', () => {
  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;
  const initialState = {};
  let store: MockStore;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductListComponent],
      providers: [provideMockStore({initialState})]
    })
      .compileComponents();
    store = TestBed.inject(MockStore);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListComponent);
    component = fixture.componentInstance;
    component.selectedProducts = new Set<Product>().add(productsStub[0]);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add product on select checked', () => {
    spyOn(store, 'dispatch');
    component.onSelect({checked: true} as MatCheckboxChange, productsStub[1]);
    fixture.detectChanges();
    expect(component.selectedProducts.size).toEqual(2);
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('should remove product on select unchecked', () => {
    spyOn(store, 'dispatch');
    component.onSelect({checked: false} as MatCheckboxChange, productsStub[1]);
    fixture.detectChanges();
    expect(component.selectedProducts.size).toEqual(1);
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });
});
