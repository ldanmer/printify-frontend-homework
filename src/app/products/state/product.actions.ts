import {Action} from '@ngrx/store';
import {Product} from '../product';

export enum ProductActionTypes {
  Add = '[Product] Add',
  Clear = '[Product] Clear',
  AddToOrder = '[Product] AddToOrder',
  ClearOrder = '[Product] ClearOrder',

}

export class Add implements Action {
  readonly type = ProductActionTypes.Add;

  constructor(public payload: Set<Product>) {
  }
}

export class Clear implements Action {
  readonly type = ProductActionTypes.Clear;
}

export class AddToOrder implements Action {
  readonly type = ProductActionTypes.AddToOrder;

  constructor(public payload: Product) {
  }
}


export class ClearOrder implements Action {
  readonly type = ProductActionTypes.ClearOrder;
}

export type ProductActions = Add
  | Clear
  | AddToOrder
  | ClearOrder;


