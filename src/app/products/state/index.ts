import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../state/app.state';
import * as fromProducts from './product.reducer';

export interface State extends fromRoot.State {
  products: fromProducts.ProductState;
}

// Selector functions
const getOrderFeatureState = createFeatureSelector<fromProducts.ProductState>('products');

export const getProducts = createSelector(
  getOrderFeatureState,
  state => state.products
);

export const getOrderedProducts = createSelector(
  getOrderFeatureState,
  state => state.orderedProducts
);

export const getError = createSelector(
  getOrderFeatureState,
  state => state.error
);
