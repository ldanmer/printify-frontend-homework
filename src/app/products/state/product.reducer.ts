import {ProductActionTypes, ProductActions} from './product.actions';
import {Product} from '../product';

export interface ProductState {
  products: Product[];
  orderedProducts: Product[];
  error: string;
}

const initialState: ProductState = {
  products: [],
  orderedProducts: [],
  error: ''
};

export function reducer(state = initialState, action: ProductActions): ProductState {
  switch (action.type) {
    case ProductActionTypes.Add:
      return {
        ...state,
        products: Array.from(action.payload)

      };

    case ProductActionTypes.Clear:
      return {
        ...state,
        products: []
      };

    case ProductActionTypes.AddToOrder:
      const orderedProducts = state.orderedProducts.filter(ordered => action.payload.id !== ordered.id);
      return {
        ...state,
        orderedProducts: [...orderedProducts, action.payload]
      };

    case ProductActionTypes.ClearOrder:
      return {
        ...state,
        orderedProducts: []
      };

    default:
      return state;
  }
}
