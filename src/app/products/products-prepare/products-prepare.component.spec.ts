import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductsPrepareComponent} from './products-prepare.component';
import {SockVariants, Variant} from '../product';

describe('ProductsPrepareComponent', () => {
  let component: ProductsPrepareComponent;
  let fixture: ComponentFixture<ProductsPrepareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductsPrepareComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsPrepareComponent);
    component = fixture.componentInstance;
    component.selectedVariants = [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return non selected message', () => {
    const nonSelectedVariant = [{
      name: SockVariants.Light,
      price: 30
    }] as Variant[];
    const message = component.variantSelected(nonSelectedVariant);
    expect(message).toEqual('non selected');
  });

});
