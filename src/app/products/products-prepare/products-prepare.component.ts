import {Component, Input} from '@angular/core';
import {Product, Variant} from '../product';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-products-prepare',
  templateUrl: './products-prepare.component.html',
  styleUrls: ['./products-prepare.component.scss']
})
export class ProductsPrepareComponent {

  @Input() selectedProducts: Product[];
  @Input() selectedVariants: Variant[];

  constructor() {
  }

  variantSelected(variants: Variant[]): string {
    const arrayInterception = this.selectedVariants.filter(variant => variants.includes(variant));
    return arrayInterception.length ? 'selected' : 'non selected';
  }

  private variantsIntersection(variants: Variant[], selected: Variant[]): Variant[] {
    return variants.filter(a => selected.some(b => a.name === b.name));
  }
}
