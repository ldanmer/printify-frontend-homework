import {Fulfillment, Order} from './orders/order';
import {SockVariants} from './products/product';

export const orders: Order[] = [
  {
    id: 1234,
    customer: 'Agronom syn Agronoma',
    created: new Date('March 15, 2020 16:40:00'),
    revenue: 5.15,
    cost: 15.15,
    price: 10,
    fulfillment: Fulfillment.InProduction,
    products: [
      {
        id: 1,
        name: 'Christmas dog socks',
        SKU: 'SOCK_101',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {
        id: 2,
        name: 'Christmas cat socks',
        SKU: 'SOCK_102',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      }
    ],
    listed: true,
    volume: 45,
    SKU: 'SKU_01'
  },
  {
    id: 1235,
    customer: 'Logovaz Tranduilov',
    created: new Date('March 16, 2020 12:40:00'),
    revenue: 6,
    cost: 54.2,
    price: 8,
    fulfillment: Fulfillment.QualityControl,
    products: [
      {
        id: 2,
        name: 'Christmas cat socks',
        SKU: 'SOCK_102',
        variants: [{name: SockVariants.HeavyWool, price: 70}, {name: SockVariants.Light, price: 30}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: true,
    volume: 41,
    SKU: 'SKU_02'
  },
  {
    id: 1236,
    customer: 'Fedor Sumkin',
    created: new Date('April 1, 2020 10:55:00'),
    revenue: 43,
    cost: 15,
    price: 80,
    fulfillment: Fulfillment.Pending,
    products: [
      {
        id: 1,
        name: 'Christmas dog socks',
        SKU: 'SOCK_101',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: false,
    volume: 23,
    SKU: 'SKU_03'
  },
  {
    id: 1237,
    customer: 'Senya Ganjubas',
    created: new Date('April 1, 2020 10:25:00'),
    revenue: 1,
    cost: 23,
    price: 18,
    fulfillment: Fulfillment.Completed,
    products: [
      {
        id: 4,
        name: 'Christmas bird socks',
        SKU: 'SOCK_104',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: false,
    volume: 5,
    SKU: 'SKU_04'
  },
  {
    id: 1238,
    customer: 'Pendalf Seriy',
    created: new Date('April 12, 2020 11:23:00'),
    revenue: 32,
    cost: 51.5,
    price: 8.2,
    fulfillment: Fulfillment.Completed,
    products: [
      {
        id: 5,
        name: 'Halloween bird socks',
        SKU: 'SOCK_105',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}]
      },
      {
        id: 6,
        name: 'Halloween mouse socks',
        SKU: 'SOCK_106',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      }
    ],
    listed: false,
    volume: 125,
    SKU: 'SKU_05'
  },
  {
    id: 1239,
    customer: 'Givi syn Zuraba',
    created: new Date('April 23, 2020 01:25:00'),
    revenue: 11.5,
    cost: 23.5,
    price: 80,
    fulfillment: Fulfillment.QualityControl,
    products: [
      {
        id: 5,
        name: 'ThanksKilling goat socks',
        SKU: 'SOCK_107',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {id: 6, name: 'Halloween bug socks', SKU: 'SOCK_108', variants: [{name: SockVariants.Light, price: 30}]}
    ],
    listed: false,
    volume: 21,
    SKU: 'SKU_06'
  },
  {
    id: 1234,
    customer: 'Agronom syn Agronoma',
    created: new Date('March 15, 2020 16:40:00'),
    revenue: 5.15,
    cost: 15.15,
    price: 10,
    fulfillment: Fulfillment.InProduction,
    products: [
      {
        id: 1,
        name: 'Christmas dog socks',
        SKU: 'SOCK_101',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {
        id: 2,
        name: 'Christmas cat socks',
        SKU: 'SOCK_102',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      }
    ],
    listed: true,
    volume: 45,
    SKU: 'SKU_01'
  },
  {
    id: 1235,
    customer: 'Logovaz Tranduilov',
    created: new Date('March 16, 2020 12:40:00'),
    revenue: 6,
    cost: 54.2,
    price: 8,
    fulfillment: Fulfillment.QualityControl,
    products: [
      {
        id: 2,
        name: 'Christmas cat socks',
        SKU: 'SOCK_102',
        variants: [{name: SockVariants.HeavyWool, price: 70}, {name: SockVariants.Light, price: 30}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: true,
    volume: 41,
    SKU: 'SKU_02'
  },
  {
    id: 1236,
    customer: 'Fedor Sumkin',
    created: new Date('April 1, 2020 10:55:00'),
    revenue: 43,
    cost: 15,
    price: 80,
    fulfillment: Fulfillment.Pending,
    products: [
      {
        id: 1,
        name: 'Christmas dog socks',
        SKU: 'SOCK_101',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: false,
    volume: 23,
    SKU: 'SKU_03'
  },
  {
    id: 1237,
    customer: 'Senya Ganjubas',
    created: new Date('April 1, 2020 10:25:00'),
    revenue: 1,
    cost: 23,
    price: 18,
    fulfillment: Fulfillment.Completed,
    products: [
      {
        id: 4,
        name: 'Christmas bird socks',
        SKU: 'SOCK_104',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: false,
    volume: 5,
    SKU: 'SKU_04'
  },
  {
    id: 1238,
    customer: 'Pendalf Seriy',
    created: new Date('April 12, 2020 11:23:00'),
    revenue: 32,
    cost: 51.5,
    price: 8.2,
    fulfillment: Fulfillment.Completed,
    products: [
      {
        id: 5,
        name: 'Halloween bird socks',
        SKU: 'SOCK_105',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}]
      },
      {
        id: 6,
        name: 'Halloween mouse socks',
        SKU: 'SOCK_106',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      }
    ],
    listed: false,
    volume: 125,
    SKU: 'SKU_05'
  },
  {
    id: 1239,
    customer: 'Givi syn Zuraba',
    created: new Date('April 23, 2020 01:25:00'),
    revenue: 11.5,
    cost: 23.5,
    price: 80,
    fulfillment: Fulfillment.QualityControl,
    products: [
      {
        id: 5,
        name: 'ThanksKilling goat socks',
        SKU: 'SOCK_107',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {id: 6, name: 'Halloween bug socks', SKU: 'SOCK_108', variants: [{name: SockVariants.Light, price: 30}]}
    ],
    listed: false,
    volume: 21,
    SKU: 'SKU_06'
  },
  {
    id: 1234,
    customer: 'Agronom syn Agronoma',
    created: new Date('March 15, 2020 16:40:00'),
    revenue: 5.15,
    cost: 15.15,
    price: 10,
    fulfillment: Fulfillment.InProduction,
    products: [
      {
        id: 1,
        name: 'Christmas dog socks',
        SKU: 'SOCK_101',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {
        id: 2,
        name: 'Christmas cat socks',
        SKU: 'SOCK_102',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      }
    ],
    listed: true,
    volume: 45,
    SKU: 'SKU_01'
  },
  {
    id: 1235,
    customer: 'Logovaz Tranduilov',
    created: new Date('March 16, 2020 12:40:00'),
    revenue: 6,
    cost: 54.2,
    price: 8,
    fulfillment: Fulfillment.QualityControl,
    products: [
      {
        id: 2,
        name: 'Christmas cat socks',
        SKU: 'SOCK_102',
        variants: [{name: SockVariants.HeavyWool, price: 70}, {name: SockVariants.Light, price: 30}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: true,
    volume: 41,
    SKU: 'SKU_02'
  },
  {
    id: 1236,
    customer: 'Fedor Sumkin',
    created: new Date('April 1, 2020 10:55:00'),
    revenue: 43,
    cost: 15,
    price: 80,
    fulfillment: Fulfillment.Pending,
    products: [
      {
        id: 1,
        name: 'Christmas dog socks',
        SKU: 'SOCK_101',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: false,
    volume: 23,
    SKU: 'SKU_03'
  },
  {
    id: 1237,
    customer: 'Senya Ganjubas',
    created: new Date('April 1, 2020 10:25:00'),
    revenue: 1,
    cost: 23,
    price: 18,
    fulfillment: Fulfillment.Completed,
    products: [
      {
        id: 4,
        name: 'Christmas bird socks',
        SKU: 'SOCK_104',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}]
      },
      {
        id: 3,
        name: 'Christmas mouse socks',
        SKU: 'SOCK_103',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      }
    ],
    listed: false,
    volume: 5,
    SKU: 'SKU_04'
  },
  {
    id: 1238,
    customer: 'Pendalf Seriy',
    created: new Date('April 12, 2020 11:23:00'),
    revenue: 32,
    cost: 51.5,
    price: 8.2,
    fulfillment: Fulfillment.Completed,
    products: [
      {
        id: 5,
        name: 'Halloween bird socks',
        SKU: 'SOCK_105',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}]
      },
      {
        id: 6,
        name: 'Halloween mouse socks',
        SKU: 'SOCK_106',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
      }
    ],
    listed: false,
    volume: 125,
    SKU: 'SKU_05'
  },
  {
    id: 1239,
    customer: 'Givi syn Zuraba',
    created: new Date('April 23, 2020 01:25:00'),
    revenue: 11.5,
    cost: 23.5,
    price: 80,
    fulfillment: Fulfillment.QualityControl,
    products: [
      {
        id: 5,
        name: 'ThanksKilling goat socks',
        SKU: 'SOCK_107',
        variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
          name: SockVariants.Light,
          price: 30
        }]
      },
      {id: 6, name: 'Halloween bug socks', SKU: 'SOCK_108', variants: [{name: SockVariants.Light, price: 30}]}
    ],
    listed: false,
    volume: 21,
    SKU: 'SKU_06'
  }
];
