import {Component, OnInit} from '@angular/core';
import {OrderService} from './orders/order.service';
import {OrderDataSource} from './orders/order.data-source';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Orders';
  dataSource: OrderDataSource;

  constructor(private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.dataSource = new OrderDataSource(this.orderService);
    this.dataSource.loadOrders();
  }
}
