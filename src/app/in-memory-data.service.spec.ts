import {TestBed} from '@angular/core/testing';

import {InMemoryDataService} from './in-memory-data.service';
import {Order} from './orders/order';

describe('InMemoryDataService', () => {
  let service: InMemoryDataService;
  let orders: Order[];

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InMemoryDataService);
    // @ts-ignore
    orders = service.createDb().orders;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return 6 orders', () => {
    expect(orders.length).toEqual(18);
  });
});
