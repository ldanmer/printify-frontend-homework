import {Product, SockVariants} from '../../products/product';

export const productsStub: Product[] = [
  {
    id: 1,
    name: 'Christmas dog socks',
    SKU: 'SOCK_101',
    variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
      name: SockVariants.Light,
      price: 30
    }]
  },
  {
    id: 2,
    name: 'Christmas cat socks',
    SKU: 'SOCK_102',
    variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.HeavyWool, price: 70}, {
      name: SockVariants.Light,
      price: 30
    }]
  },
  {
    id: 3,
    name: 'Christmas mouse socks',
    SKU: 'SOCK_103',
    variants: [{name: SockVariants.Crew, price: 50}, {name: SockVariants.Light, price: 30}]
  }
];
