import {Fulfillment, Order} from '../../orders/order';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';


export const ordersStub = [
  {
    id: 1234,
    customer: 'Agronom syn Agronoma',
    created: new Date('March 15, 2020 16:40:00'),
    revenue: 5.15,
    cost: 15.15,
    price: 10,
    fulfillment: Fulfillment.InProduction,
    products: [],
    listed: true
  },
  {
    id: 1235,
    customer: 'Logovaz Tranduilov',
    created: new Date('March 16, 2020 12:40:00'),
    revenue: 6,
    cost: 54.2,
    price: 8,
    fulfillment: Fulfillment.QualityControl,
    products: [],
    listed: true
  },
  {
    id: 1236,
    customer: 'Fedor Sumkin',
    created: new Date('April 1, 2020 10:55:00'),
    revenue: 43,
    cost: 15,
    price: 80,
    fulfillment: Fulfillment.Pending,
    products: [],
    listed: false
  },
] as Order[];

@Injectable({
  providedIn: 'root'
})
export class OrderServiceMock {
  getListedOrders(): Observable<Order[]> {
    return of(ordersStub)
      .pipe(
        map(orders => orders.filter(order => !!order.listed))
      );
  }

  searchUnlistedOrdersById(id: string): Observable<Order[]> {
    return of([ordersStub[2]]);
  }
}
